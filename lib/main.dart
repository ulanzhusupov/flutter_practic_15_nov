import 'package:flutter/material.dart';
import 'package:flutter_practic_15_nov/presentation/screens/home_screen.dart';
import 'package:flutter_practic_15_nov/presentation/theme/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(390, 800),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            scaffoldBackgroundColor: AppColors.white,
            appBarTheme: const AppBarTheme(
                backgroundColor: AppColors.white, elevation: 0),
            colorScheme: ColorScheme.fromSeed(seedColor: AppColors.darkBlue),
            useMaterial3: true,
          ),
          home: const HomeScreen(),
        );
      },
    );
  }
}
