import 'package:flutter/material.dart';

abstract class AppFonts {
  static const TextStyle s48W400 = TextStyle(
    fontSize: 48,
    fontWeight: FontWeight.w400,
  );
  static const TextStyle s16W400 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );
  static const TextStyle s11W400 = TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.w400,
  );
  static const TextStyle s20W700 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w700,
  );
  static const TextStyle s16W300 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w300,
  );
  static const TextStyle s15W300 = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w300,
  );
  static const TextStyle s13W700 = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w700,
  );
}
