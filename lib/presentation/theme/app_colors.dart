import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color darkBlue = Color(0xff2B4C59);
  static const Color black = Color(0xff000000);
  static const Color white = Color(0xffffffff);
  static const Color grey = Color(0xffA1A1A1);
  static const Color yellow = Color(0xffFCC21B);
}
