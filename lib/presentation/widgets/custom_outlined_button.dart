import 'package:flutter/material.dart';
import 'package:flutter_practic_15_nov/presentation/theme/app_colors.dart';
import 'package:flutter_practic_15_nov/presentation/theme/app_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomOutlinedButton extends StatelessWidget {
  const CustomOutlinedButton({
    super.key,
    required this.title,
    required this.imgPath,
  });

  final String title;
  final String imgPath;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
        side: BorderSide(
          width: 2.0.w,
          color: AppColors.black,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      onPressed: () {},
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 17),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              imgPath,
              width: 31.w,
            ),
            Text(
              title,
              style: AppFonts.s16W400.copyWith(
                fontFamily: "Imprima",
                color: AppColors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
