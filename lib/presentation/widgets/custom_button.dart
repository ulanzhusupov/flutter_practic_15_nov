import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButtons extends StatelessWidget {
  const CustomButtons({
    super.key,
    required this.title,
    this.bgColor = Colors.transparent,
    required this.btnStyle,
  });

  final String title;
  final Color bgColor;
  final TextStyle btnStyle;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        style: ButtonStyle(
          overlayColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.pressed)) {
              return const Color.fromARGB(255, 80, 123, 139);
            }
            return null;
          }),
          backgroundColor: MaterialStatePropertyAll(bgColor),
          shape: MaterialStatePropertyAll(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0.r),
            ),
          ),
        ),
        onPressed: () {},
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 14),
          child: Text(
            title,
            style: btnStyle,
          ),
        ),
      ),
    );
  }
}
