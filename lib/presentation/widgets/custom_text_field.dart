import 'package:flutter/material.dart';
import 'package:flutter_practic_15_nov/presentation/theme/app_colors.dart';
import 'package:flutter_practic_15_nov/presentation/theme/app_fonts.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.labelText,
    required this.hintText,
    this.obscureText = false,
    this.onSuffixPressed,
  });

  final String labelText;
  final String hintText;
  final bool obscureText;
  final Function()? onSuffixPressed;

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: obscureText,
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.always,
        labelText: labelText,
        floatingLabelStyle: AppFonts.s16W400.copyWith(
          fontFamily: "Roboto",
          color: AppColors.darkBlue,
        ),
        hintText: hintText,
        hintStyle: AppFonts.s16W400.copyWith(
          fontFamily: "Roboto",
          color: AppColors.grey,
        ),
        suffixIcon: labelText == "PASSWORD"
            ? IconButton(
                icon: obscureText
                    ? const Icon(Icons.visibility_off_outlined)
                    : const Icon(Icons.visibility_outlined),
                onPressed: onSuffixPressed ?? () {},
              )
            : const SizedBox(),
      ),
    );
  }
}
