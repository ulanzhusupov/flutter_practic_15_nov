import 'package:flutter/material.dart';
import 'package:flutter_practic_15_nov/presentation/theme/app_colors.dart';
import 'package:flutter_practic_15_nov/presentation/theme/app_fonts.dart';
import 'package:flutter_practic_15_nov/presentation/widgets/custom_button.dart';
import 'package:flutter_practic_15_nov/presentation/widgets/custom_outlined_button.dart';
import 'package:flutter_practic_15_nov/presentation/widgets/custom_text_field.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool obscureText = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 100.h),
              Text("Sign In",
                  style: AppFonts.s48W400.copyWith(fontFamily: "Imprima")),
              SizedBox(height: 87.h),
              const CustomTextField(
                labelText: "EMAIL OR PHONE",
                hintText: "Loremipsum@gmail.com",
              ),
              SizedBox(height: 28.h),
              CustomTextField(
                labelText: "PASSWORD",
                hintText: "*******",
                obscureText: !obscureText,
                onSuffixPressed: () {
                  setState(() {
                    obscureText = !obscureText;
                  });
                },
              ),
              SizedBox(height: 12.h),
              TextButton(
                style: TextButton.styleFrom(padding: EdgeInsets.all(0)),
                onPressed: () {},
                child: Text(
                  "Forgot password?",
                  style: AppFonts.s11W400.copyWith(
                    fontFamily: "Inter",
                    color: AppColors.black,
                  ),
                ),
              ),
              SizedBox(height: 34.h),
              CustomButtons(
                title: "Log In",
                bgColor: AppColors.darkBlue,
                btnStyle: AppFonts.s20W700.copyWith(
                  fontFamily: "Inter",
                  color: AppColors.white,
                ),
              ),
              SizedBox(height: 9.h),
              Center(
                child: Text(
                  "OR",
                  style: AppFonts.s16W300.copyWith(
                    fontFamily: "Inter",
                    color: AppColors.black,
                  ),
                ),
              ),
              SizedBox(height: 9.h),
              const CustomOutlinedButton(
                title: "Continue With Google",
                imgPath: "assets/icons/google_logo.png",
              ),
              SizedBox(height: 13.h),
              const CustomOutlinedButton(
                title: "Continue With Facebook",
                imgPath: "assets/icons/facebook_logo.png",
              ),
              SizedBox(height: 51.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Don’t Have an account yet?",
                    style: AppFonts.s15W300.copyWith(
                      fontFamily: "Inter",
                      color: AppColors.black,
                    ),
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "SIGN UP",
                      style: AppFonts.s13W700.copyWith(
                          fontFamily: "Inter",
                          color: AppColors.yellow,
                          fontStyle: FontStyle.italic),
                    ),
                  )
                ],
              ),
              SizedBox(height: 100.h)
            ],
          ),
        ),
      ),
    );
  }
}
